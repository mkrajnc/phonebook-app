﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AddresBook.Models
{

    public partial class Person
    {
        public Person(string name, string surename, string city, string gender)
        {
            FirstName = name;
            SecondName = surename;
            Gender = gender;
            City = city;
        }

        public long ContactId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
    }

    // we need this to group returning list by first letter of the name
    public class AlphabeticalMapping<T>
    {
        public char FirstLetter { get; set; }
        public List<T> Items { get; set; }
    }

    // we need this to group returning list by first letter of the name
    public class AlphabeticalList<T>
    {
        public char Letter { get; set; }
    }
}