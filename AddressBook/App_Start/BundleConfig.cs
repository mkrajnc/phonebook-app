﻿using System.Web;
using System.Web.Optimization;

namespace AddressBook
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            // css bundle
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/style.css",
                "~/Content/css/font-awesome.min.css"
            ));

            // angular library & modules bundle
            bundles.Add(new ScriptBundle("~/Content/jslibs").Include(
                
                "~/Content/js/jquery.min.js",
                "~/Content/js/jquery.slimscroll.min.js",
                "~/Content/js/underscore-min.js",

                "~/Content/js/angular/angular.min.js",
                "~/Content/js/angular/angular-ui-router.min.js",
                "~/Content/js/angular/angular-resource.min.js"
            ));

            // custom app bundle
            bundles.Add(new ScriptBundle("~/Content/app").Include(
                      "~/Content/js/app/app.js",
                      "~/Content/js/app/controllers/*.js",
                      "~/Content/js/app/services/*.js",
                      "~/Content/js/app/directives/*.js"
            ));

        }
    }
}
