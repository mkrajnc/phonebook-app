appControllers.controller('PeopleListCtrl', ['$document', '$scope', '$timeout', '$filter', 'people', 'SidebarFactory', function PeopleListCtrl($document, $scope, $timeout, $filter, people, SidebarFactory) {

    // resolved in router assign to scope
    $scope.people = people;

    // selected letter from sidebar
    $scope.selectedSidebarLetter = SidebarFactory.read();

    // watcher for sidebar letter
    $scope.$watch(function () {
        return SidebarFactory.read();
    }, function (newLetter, oldLetter) {
        if (newLetter !== oldLetter) {
            $scope.selectedSidebarLetter = newLetter;
        }
    });

    // action on person detail click
    $scope.selectItem = function selectItem(person) {
        
        _.forEach(people, function (p) {
            var single = _.find(p.items, function (obj) {
                if (obj.selected == true) {
                    obj.selected = false;
                }
            })
        });

        person.selected = true;

        // scroll to person
        var item        = $('#person-' + person.contactId),
            container   = $('#list');

        container.slimScroll({ scrollTo: container.scrollTop() + item.offset().top - container.offset().top + (options.list_offset) + 'px' });

    };

}]);

appControllers.controller('PeopleDetailsCtrl', ['$scope', '$stateParams', '$timeout', 'people', 'SidebarFactory', '_', function PeopleListCtrl($scope, $stateParams, $timeout, people, SidebarFactory, _) {
      
    // state parameters, save them for later use
    var letter  = $stateParams.letter,
        cId     = $stateParams.id;

    $scope.init = function () {

        /*
        The way JSON file is put together, we first need to find the list with first letter,
        and then search for object with contactId parameter.
        We are using Underscore (http://underscorejs.org/) for this because it makes things easy.
        */

        var list = _.findWhere(people, { firstLetter: letter });

        var person = _.find(list.items, function (obj) {
            return obj.contactId == cId;
        })

        // we have the person, assign it to scope
        $scope.person = person;

        // read and check selected letter
        var readLetter = SidebarFactory.read();

        if (readLetter != person.firstName[0]) {
            $scope.selectedSidebarLetter = SidebarFactory.select(person.firstName[0]);
        } else {
            $scope.selectedSidebarLetter = readLetter;
        }

        // check if marked active, if not mark it
        if (!person.selected) {
            person.selected = true;
        }

        // scroll to position
        var item        = $('#person-' + person.contactId),
            container   = $('#list');

        container.slimScroll({ scrollTo: container.scrollTop() + item.offset().top - container.offset().top + (options.list_offset) + 'px' });

    }

    // we need to wait for DOM
    $timeout($scope.init)

}]);