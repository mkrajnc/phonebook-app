appControllers.controller('SidebarCtrl', ['$scope', '$state', '$document', 'alphabet', 'SidebarFactory', function ListCtrl($scope, $state, $document, alphabet, SidebarFactory) {

    // resolved in router assign to scope
    $scope.alphabet = alphabet;

    // default letter is the first one
    $scope.selectedSidebarLetter = SidebarFactory.select(alphabet[0].letter);

	// select letter on click
    $scope.selectSidebarLetter = function (letter) {

        // set letter in factory, so we can read it from elsewhere
        $scope.selectedSidebarLetter = SidebarFactory.select(letter);

        // scroll to letter
        var item        = $('#list_' + letter),
            container   = $('#list');

        container.slimScroll({ scrollTo: container.scrollTop() + item.offset().top - container.offset().top + 'px' });

	};

}]);