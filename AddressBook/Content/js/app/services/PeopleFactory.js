appServices.factory('PeopleFactory', ['$resource', function ($resource) {

	return $resource(options.api_base_url + 'Persons/:id', {}, {
        query: { method: 'GET', isArray: true }
    });

}]);