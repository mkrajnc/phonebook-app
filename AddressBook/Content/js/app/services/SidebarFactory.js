﻿appServices.factory('SidebarFactory', function () {

    var selectedLetter = null;
    
    return {

        select: function (letter) {
            selectedLetter = letter;
            return selectedLetter;
        },

        read: function () {
            return selectedLetter;
        }

    }

});