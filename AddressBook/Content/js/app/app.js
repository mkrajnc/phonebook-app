var app = angular.module('saopAddressBook', ['ui.router', 'ngResource', 'appControllers', 'appServices', 'underscore', 'ui.slimscroll']);

var appControllers  = angular.module('appControllers', []);
var appServices = angular.module('appServices', []);

// global options
var options = {};
options.api_base_url = "http://localhost:63178/api/";
options.list_offset = -72;

app.config(function($stateProvider, $urlRouterProvider) {

    /*
        Thanks to ui.router we can put main resolves in main view
        and all of our child views inherit the promise. This way
        we always have data available when we load view.
    */

    $stateProvider
  	    .state(		
  		    'people', {
  		        url: '/people',
  		        resolve: {
                    // resolve sidebar
  		            PeopleFactory: 'PeopleFactory',
  		            alphabet: function (PeopleFactory) {
  		                return PeopleFactory.query({ alphabet: "alphabet" }).$promise;
  		            },
                    // resolve main list
  		            PeopleFactory: 'PeopleFactory',
  		            people: function (PeopleFactory) {
  		                return PeopleFactory.query().$promise;
  		            }
  		        },
			    views: {
				    'sidebar@': {
				        templateUrl: 'Content/partials/sidebar.html',
					    controller: 'SidebarCtrl'
				    },
		      	    'main@': {
		      	        templateUrl: 'Content/partials/people.html',
					    controller: 'PeopleListCtrl'
	      		    }
    		    }
  		    }
	    )
	    .state('people.details', {
	        url: '/:letter/:id/details',
    	    views: {
    		    'details': {
    		        templateUrl: 'Content/partials/people.details.html',
				    controller: 'PeopleDetailsCtrl'
    		    }
    	    }
	    });

	$urlRouterProvider.otherwise('/people');

});