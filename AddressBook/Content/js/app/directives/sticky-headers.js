﻿app.directive("scroll", function (SidebarFactory) {

    return function (scope, elem) {

        angular.element(elem).bind("scroll", function () {

            var titles = $(".stickyHeader");

            titles.each(function () {

                var el = $(this);

                if (el.offset().top <= $("#currentLetter").offset().top) {
                    scope.selectedSidebarLetter = SidebarFactory.select(el.html());
                    scope.$apply();
                }

            });

        });

    };

});