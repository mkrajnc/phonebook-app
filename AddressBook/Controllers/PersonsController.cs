﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AddresBook.Models;
using System.Globalization;

namespace AddressBook.Controllers
{
    public class PersonsController : ApiController
    {

        private PersonRepository _personRepository;

        private StringComparer comparerSI;

        public PersonsController()
        {
            _personRepository = new PersonRepository();
            comparerSI = StringComparer.Create(new CultureInfo("sl-SI"), true);
        }

        // get the list of all persons
        public IEnumerable<AlphabeticalMapping<Person>> Get()
        {

            // select people and group them by first letter of the name
            var peopleSorted = (
                from p in _personRepository.Entities    
                group p by p.FirstName[0]
                    into lgroup
                    select new AlphabeticalMapping<Person>()
                    {
                        FirstLetter = lgroup.Key,
                        Items = lgroup.ToList()
                    }).OrderBy(x => x.FirstLetter.ToString(), comparerSI);

            return peopleSorted;

        }

        // get the exact person
        public Person Get(long id)
        {
            return _personRepository.Get(x => x.ContactId == id);
        }

        // return alphabetical
        public IEnumerable<AlphabeticalList<Person>> Get(string alphabet)
        {
            var lettersList = (
                    from p in _personRepository.Entities
                    group p by p.FirstName[0] into g
                        select new AlphabeticalList<Person>() { Letter = g.Key }
                ).OrderBy(x => x.Letter.ToString(), comparerSI);

            return lettersList;
        }

    }
}
